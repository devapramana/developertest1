<?php

$day = array(
    "Monday"=>"Mon", 
    "Tuesday"=>"Tue", 
    "Wednesday"=>"Wed", 
    "Thursday"=>"Thu", 
    "Friday"=>"Fri", 
    "Saturday"=>"Sat", 
    "Sunday"=>"Sun"
);

$date=date("D");

?>
<!DOCTYPE html>
<head>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <div class="main-container">
        <div class="text-center">
            <?php foreach($day as $i => $item) {
                 if ($date==$item)
                {
                    echo"<p>Today is <span id=\"thisDay\">".$i."</span></p>";
                }
                ?>
            <?php } ?>
        </div>
        <div class="btn-group">
            <?php foreach($day as $i => $item) { ?>
                <button onclick="clickbtn<?=$item?>()" id="<?=$item?>" class="btn <?php if ($date==$item)
                {
                    echo'today';
                }
                ?>"><?=$i; ?></button>
            <?php } ?>
        </div>
        <div class="text-center">
            <p>Selected day is <span id="selectedDay"></span></p>
        </div>
    </div>

    <script>
        <?php foreach($day as $i => $item) { ?>
        function clickbtn<?=$item?>() {
            var element=document.getElementsByClassName("btn");
            for (let i = 0; i < element.length; i++) {
                element[i].style.border = "";
                element[i].style.boxShadow = "";
            }
            document.getElementById("<?=$item?>").style.border = "solid 1px white";
            document.getElementById("<?=$item?>").style.boxShadow = "0px 0px 11px 0px #565656";
            document.getElementById("selectedDay").innerHTML = "<?=$i?>";
        }
        <?php } ?>
    </script>
</body>